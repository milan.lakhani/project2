def score(word):
	scrabble_dict = {"A":1,"E":1,"I":1,"O":1,"U":1,
	"L":1,"N":1,"R":1,"S":1,"T":1,"D":2,"G":2,
	"B":3,"C":3,"M":3,"P":3,"F":4,"H":4,"V":4,
	"W":4,"Y":4,"K":5,"J":8,"X":8,"Q":10,"Z":10}
	WORD = word.upper()
	score = 0
	for letter in WORD:
		score += scrabble_dict[letter]
	return(score)

def scrabble(word, doub_letter="", trip_letter="",
	blank_letter={}, doub_word=0, trip_word=0):
	scrabble_dict = {"A":1,"E":1,"I":1,"O":1,"U":1,
	"L":1,"N":1,"R":1,"S":1,"T":1,"D":2,"G":2,
	"B":3,"C":3,"M":3,"P":3,"F":4,"H":4,"V":4,
	"W":4,"Y":4,"K":5,"J":8,"X":8,"Q":10,"Z":10}
	WORD = word.upper()
	score = 0
	for letter in WORD:
		score += scrabble_dict[letter]
	if doub_letter != "":
		DOUB = doub_letter.upper()
		for letter in DOUB:
			score += scrabble_dict[letter]
	if trip_letter != "":
		TRIP = trip_letter.upper()
		for letter in TRIP:
			score += 2*scrabble_dict[letter]
	if blank_letter != {}:
		for key in blank_letter:
			score += (1 - scrabble_dict[key])*blank_letter[key]
	score = score*2**doub_word*3**trip_word
	return(score)
